<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>
    <?php
    echo "<h3> Soal 1 </h3>";
    $kids = ['Mike', 'Dustin', 'Will', 'Lucas', 'Max', 'Eleven']; // Lengkapi di sini
    $adults = ['Hopper', 'Nancy', 'Joyce', 'Jonathan', 'Murray'];
    echo "$kids[1] <br>";
    echo $adults[1];

    echo "<h3> Soal 2</h3>";
    $jumlahkids = count($kids);
    $jumlahdults = count($adults);
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: $jumlahkids"; // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";
    // Lanjutkan
    echo "</ol>";
    echo "Total Adults: $jumlahadults"; // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    // Lanjutkan
    echo "</ol>";

    echo "<h3>Soal 3</h3>";
    $datadiri['0'] = "Name : Will Blayers <br>
                      Age : 12 <br>
                      Aliases : Will the wise <br>
                      Status : Alive";
    $datadiri['1'] = "Name : Mike Wheeler <br>
                      Age : 12 <br>
                      Aliases : Dugeon Master <br>
                      Status : Alive";
    $datadiri['2'] = "Name : Jim Hooper <br>
                      Age : 43 <br>
                      Aliases : Chief Hooper <br>
                      Status : Deceased";
    $datadiri['3'] = "Name : Eleven <br>
                      Age : 12 <br>
                      Aliases : EL <br>
                      Status : Alive";
    echo "$datadiri[2] <br>";
    echo $datadiri[3];
    ?>
</body>

</html>
